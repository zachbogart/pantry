# Pantry

Provides base docker containers with jupyter and some common installs for Python and R

- Available on [devperiscopic DockerHub](https://hub.docker.com/r/devperiscopic/pantry)

***

# Containers

| Container | Usage | Dockerfile | Python | R | Description
| :---: | :--- | :---: | :---: | :---: | :--- |
| [classic](https://bitbucket.org/zachbogart/pantry/src/main/classic/) | `FROM devperiscopic/pantry:classic` | [Dockerfile](https://bitbucket.org/zachbogart/pantry/src/main/classic/Dockerfile) | [Pipfile](https://bitbucket.org/zachbogart/pantry/src/main/classic/Pipfile) | [R Packages](https://bitbucket.org/zachbogart/pantry/src/main/classic/install_packages.R) | The basics for Python and R. 
| [prepared](https://bitbucket.org/zachbogart/pantry/src/main/prepared) | `FROM devperiscopic/pantry:prepared` | [Dockerfile](https://bitbucket.org/zachbogart/pantry/src/main/prepared/Dockerfile) | [Pipfile](https://bitbucket.org/zachbogart/pantry/src/main/prepared/Pipfile) | [R Packages](https://bitbucket.org/zachbogart/pantry/src/main/prepared/install_packages.R) | Copy of `classic` with selected nbextentions pre-installed. 
| [scribe](https://bitbucket.org/zachbogart/pantry/src/main/scribe) | `FROM devperiscopic/pantry:scribe` | [Dockerfile](https://bitbucket.org/zachbogart/pantry/src/main/scribe/Dockerfile) | [Pipfile](https://bitbucket.org/zachbogart/pantry/src/main/scribe/Pipfile) | [R Packages](https://bitbucket.org/zachbogart/pantry/src/main/scribe/install_packages.R) | Copy of `prepared`, includes Python GQL client module for interacting with databases

***  

## Why pantry?

Pantry: it's like a collection of different spices you can use for cooking up cool projects. Plus it's a lovely word.

Made with 💖